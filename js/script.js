var appJS = (function ($) {
    return {
        init: function () {
            navJS.getTasks();
            this.addEventListeners();

        },

        addEventListeners: function () {
            $(document).on('click', '.nav', function (e) {
                aceJS.loadTaskEvent( e );
            });

            $(document).keydown(function (e) {
                aceJS.validateCode(e);
            });
        }
    }
})($);

var navJS = (function ($) {
    return{
        getTasks: function () {
            var _this = this;
            $.when( tasksJS.getTasksAjax() )
                .then(function (data) {
                    tasksJS.data = data;
                    _this.loadNav( data );
                    //load first seq task info
                    aceJS.loadTask( 0, 0 );
                });
        },

        loadNav: function (data) {
            var items = [];
            $.each( data, function (i, seq) {
                items.push('<li><a id="' + seq.id + '" data-index="' + i + '" href="' + seq.id + '">' + seq.name + '</a></li>');

                $.each(seq.tasks, function (k, task) {
                    items.push('<li><a id="' + task.id + '" data-seq-index="' + i + '" data-task-index="' + k + '" data-seq="' + seq.id + '" href=" ' + task.id + '">' + task.name + '</a></li>');
                });
            });

            $('#nav').append(items.join(''));

        }
    }

})($);

var tasksJS = (function ($) {
    return{
        data: '',
        
        getById: function( seqId, taskId ){
            return { seq: this.data[seqId], task: this.data[seqId].tasks[taskId]};
        },

        getTasksAjax: function () {
            var url = 'https://gist.githubusercontent.com/thireven/841b585de43a7d708bff/raw/a6978db634551fc72633e10a31ca509c1ba0881d/sequence_sample.json'
            return $.getJSON(url);
        }
    }

})($);

var aceJS = (function ($) {
    return {
        init: function () {
            this.editor = ace.edit("editor");
            this.editor.setTheme("ace/theme/monokai");
            this.editor.getSession().setMode("ace/mode/html");
            this.showEditor();
            this.active = true;
        },

        loadTaskEvent: function( e ){
            e.preventDefault();
            var target = $(e.target);
            var seqId = +target.attr('data-seq-index');
            var taskId = +target.attr('data-task-index');

            this.loadTask( seqId, taskId );
        },

        loadTask: function( seqId, taskId ){
            var task = tasksJS.getById( seqId, taskId );
            this.selectedTask = { seqIndex: seqId, taskIndex: taskId, task: task };
            this.selectedTaskType = this.selectedTask.task.task.options.type;

            //Set Header
            $('#taskHeader').text( task.seq.name + ' ' + task.task.name );

            //Only editor if type is code
            if( this.selectedTaskType === 'info'){

                this.loadInfoContainer( task.task.lines );
                this.removeEditor();
            }
            else {
                //Prepare info
                var lines = [];
                $.each(task.task.lines, function(i, item){
                    lines.push(item);
                });
                this.loadInfoContainer( lines.join('\n') )

                //Show editor
                this.init();
            }
        },

        validateCode: function (e) {
            if ( e.which !== 13 ) return;

            if ( this.active) {
                var info = $.trim( $('#info').text() );
                var code = $.trim( this.editor.getSession().getValue() );
                if( info === code ){
                    alert('complete')
                    this.nextTask();
                    this.editor.getSession().setValue('');
                }

            }
            else{
                this.nextTask();
            }
        },

        nextTask: function(){
            var seqIndex = this.selectedTask.seqIndex;
            var end = tasksJS.data[seqIndex].tasks.length === this.selectedTask.taskIndex +1;
            seqIndex = end ? this.selectedTask.seqIndex + 1 : this.selectedTask.seqIndex;
            taskIndex = end ? 1 : this.selectedTask.taskIndex +1;
            this.selectedTaskType = 'code';
            this.loadTask( seqIndex, taskIndex );
        },

        removeEditor: function(){
            $('#editor').addClass('hide-editor');
            this.active = false;
        },

        showEditor: function(){
            $('#editor').addClass('ace-monokai').removeClass('hide-editor');
        },

        loadInfoContainer: function( text ){
            $('pre').html( text );
        }
    }

})($);

$(document).ready(function () {
    appJS.init();
});